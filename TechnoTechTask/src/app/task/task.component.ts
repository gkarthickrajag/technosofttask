import { Component, OnInit } from '@angular/core';
import {task} from '../taskmodel'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  l: any;
  title = "karthick"
  mainstring: "";
  str1 = [];
  str2 = [];
  task = new task();
  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    this.mainstring = form.value.mainstring
    this.l = this.mainstring.length;
    console.log(this.l)
    for (let i = 0; i<=this.l ; i++) {
      if (i%2 == 0) {
        this.str1.push(this.mainstring[i]);
      }
      else {
        this.str2.push(this.mainstring[i]);
      }
     console.log(this.str1);
     console.log(this.str2);
    }
  }
}
